// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "FunctionalSet",
    products: [
        .library(name: "FunctionalSet", targets: ["FunctionalSet"]),
    ],
    targets: [
        .target(name: "FunctionalSet"),
        .testTarget(name: "FunctionalSetTests", dependencies: ["FunctionalSet"]),
    ]
)
