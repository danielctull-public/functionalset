
public struct FSet<Element> {
    public let contains: (Element) -> Bool

    public init(contains: @escaping (Element) -> Bool) {
        self.contains = contains
    }
}

extension FSet {

    public static var empty: FSet {
        FSet { _ in false }
    }

    public static var universe: FSet {
        FSet { _ in true }
    }
}

extension FSet where Element: Equatable {

    public init(_ element: Element) {
        self.init { $0 == element }
    }

    public init(_ elements: Element...) {
        self = elements.reduce(.empty) { $0.union(FSet($1)) }
    }
}

extension FSet {

    public func contramap<NewElement>(
        _ transform: @escaping (NewElement) -> Element
    ) -> FSet<NewElement> {
        FSet<NewElement> { self.contains(transform($0)) }
    }
}

extension FSet {

    public var complement: FSet {
        FSet { !self.contains($0) }
    }

    public func union(_ other: FSet) -> FSet {
        FSet { self.contains($0) || other.contains($0) }
    }

    public func intersection(_ other: FSet) -> FSet {
        FSet { self.contains($0) && other.contains($0) }
    }

    public func difference(_ other: FSet) -> FSet {
        FSet { self.contains($0) && !other.contains($0) }
    }

    public func symmetricDifference(_ other: FSet) -> FSet {
        difference(other).union(other.difference(self))
    }

    public func cartesianProduct<Other>(_ other: FSet<Other>) -> FSet<(Element, Other)> {
        FSet<(Element, Other)> { self.contains($0.0) && other.contains($0.1) }
    }
}
