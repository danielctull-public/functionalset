
import XCTest
import FunctionalSet

enum Item: Equatable {
    case a, b, c, d, e
}

final class FSetTests: XCTestCase {

    func testEmpty() {
        let empty = FSet<Item>.empty
        XCTAssertEqual(empty.contains(.a), false)
        XCTAssertEqual(empty.contains(.b), false)
        XCTAssertEqual(empty.contains(.c), false)
        XCTAssertEqual(empty.contains(.d), false)
        XCTAssertEqual(empty.contains(.e), false)
    }

    func testUniverse() {
        let empty = FSet<Item>.universe
        XCTAssertEqual(empty.contains(.a), true)
        XCTAssertEqual(empty.contains(.b), true)
        XCTAssertEqual(empty.contains(.c), true)
        XCTAssertEqual(empty.contains(.d), true)
        XCTAssertEqual(empty.contains(.e), true)
    }

    func testInit() {
        let even = FSet<Int> { $0.isMultiple(of: 2) }
        XCTAssertEqual(even.contains(0), true)
        XCTAssertEqual(even.contains(1), false)
        XCTAssertEqual(even.contains(2), true)
        XCTAssertEqual(even.contains(3), false)
        XCTAssertEqual(even.contains(4), true)
        XCTAssertEqual(even.contains(5), false)
    }

    func testInitElement() {
        let set = FSet<Item>(.a)
        XCTAssertEqual(set.contains(.a), true)
        XCTAssertEqual(set.contains(.b), false)
        XCTAssertEqual(set.contains(.c), false)
        XCTAssertEqual(set.contains(.d), false)
        XCTAssertEqual(set.contains(.e), false)
    }

    func testInitElements() {
        let set = FSet<Item>(.a, .b, .c)
        XCTAssertEqual(set.contains(.a), true)
        XCTAssertEqual(set.contains(.b), true)
        XCTAssertEqual(set.contains(.c), true)
        XCTAssertEqual(set.contains(.d), false)
        XCTAssertEqual(set.contains(.e), false)
    }

    func testContramap() {
        let singleCharacters: FSet<String> = FSet { $0.count == 1 }
        let singleDigits: FSet<Int> = singleCharacters.contramap(String.init)
        XCTAssertEqual(singleDigits.contains(0),    true)
        XCTAssertEqual(singleDigits.contains(1),    true)
        XCTAssertEqual(singleDigits.contains(2),    true)
        XCTAssertEqual(singleDigits.contains(3),    true)
        XCTAssertEqual(singleDigits.contains(4),    true)
        XCTAssertEqual(singleDigits.contains(5),    true)
        XCTAssertEqual(singleDigits.contains(6),    true)
        XCTAssertEqual(singleDigits.contains(7),    true)
        XCTAssertEqual(singleDigits.contains(8),    true)
        XCTAssertEqual(singleDigits.contains(9),    true)
        XCTAssertEqual(singleDigits.contains(10),   false)
        XCTAssertEqual(singleDigits.contains(11),   false)
        XCTAssertEqual(singleDigits.contains(100),  false)
        XCTAssertEqual(singleDigits.contains(1984), false)
    }

    func testComplement() {
        let complement = FSet<Item>(.a, .b).complement
        XCTAssertEqual(complement.contains(.a), false)
        XCTAssertEqual(complement.contains(.b), false)
        XCTAssertEqual(complement.contains(.c), true)
        XCTAssertEqual(complement.contains(.d), true)
        XCTAssertEqual(complement.contains(.e), true)
    }

    func testUnion() {
        let union = FSet<Item>(.a).union(FSet(.c))
        XCTAssertEqual(union.contains(.a), true)
        XCTAssertEqual(union.contains(.b), false)
        XCTAssertEqual(union.contains(.c), true)
        XCTAssertEqual(union.contains(.d), false)
        XCTAssertEqual(union.contains(.e), false)
    }

    func testIntersection() {
        let intersection = FSet<Item>(.a, .b, .c).intersection(FSet(.b, .c, .d))
        XCTAssertEqual(intersection.contains(.a), false)
        XCTAssertEqual(intersection.contains(.b), true)
        XCTAssertEqual(intersection.contains(.c), true)
        XCTAssertEqual(intersection.contains(.d), false)
        XCTAssertEqual(intersection.contains(.e), false)
    }

    func testDifference() {
        let difference = FSet<Item>(.a, .b, .c).difference(FSet(.c, .d))
        XCTAssertEqual(difference.contains(.a), true)
        XCTAssertEqual(difference.contains(.b), true)
        XCTAssertEqual(difference.contains(.c), false)
        XCTAssertEqual(difference.contains(.d), false)
        XCTAssertEqual(difference.contains(.e), false)
    }

    func testSymmetricDifference() {
        let symmetricDifference = FSet<Item>(.a, .b, .c).symmetricDifference(FSet(.b, .c, .d))
        XCTAssertEqual(symmetricDifference.contains(.a), true)
        XCTAssertEqual(symmetricDifference.contains(.b), false)
        XCTAssertEqual(symmetricDifference.contains(.c), false)
        XCTAssertEqual(symmetricDifference.contains(.d), true)
        XCTAssertEqual(symmetricDifference.contains(.e), false)
    }

    func testCartesianProduct() {
        let cartesianProduct = FSet<Item>(.a, .b, .c).cartesianProduct(FSet(true))
        XCTAssertEqual(cartesianProduct.contains((.a, true)),  true)
        XCTAssertEqual(cartesianProduct.contains((.b, true)),  true)
        XCTAssertEqual(cartesianProduct.contains((.c, true)),  true)
        XCTAssertEqual(cartesianProduct.contains((.d, true)),  false)
        XCTAssertEqual(cartesianProduct.contains((.e, true)),  false)
        XCTAssertEqual(cartesianProduct.contains((.a, false)), false)
        XCTAssertEqual(cartesianProduct.contains((.b, false)), false)
        XCTAssertEqual(cartesianProduct.contains((.c, false)), false)
        XCTAssertEqual(cartesianProduct.contains((.d, false)), false)
        XCTAssertEqual(cartesianProduct.contains((.e, false)), false)
    }
}
