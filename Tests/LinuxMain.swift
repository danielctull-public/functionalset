import XCTest

import FunctionalSetTests

var tests = [XCTestCaseEntry]()
tests += FunctionalSetTests.__allTests()

XCTMain(tests)
